"""Base classes."""
import json
import importlib
import requests
from dataclasses_json import DataClassJsonMixin
from urllib.parse import urlparse, parse_qsl

class APIManager:
    """API Manager."""

    def __init__(self, host, token=None):
        self.host = host
        self.token = token
        self.headers = {
            'Content-Type': 'application/json'
        }
        if token:
            self.headers.update({'Authorization': f'Token {self.token}'})

    def get(self, endpoint, params=None):
        """Perform a GET query to the server and return JSON response."""
        return self._request('get', endpoint, params=params)

    def post(self, endpoint, data):
        """Perform a POST query to the server."""
        return self._request('post', endpoint, data=data)

    def delete(self, endpoint):
        return self._request('delete', endpoint)

    def put(self, endpoint, data):
        return self._request('put', endpoint, data=data)

    def _request(self, method, endpoint, data=None, params=None):
        url = f'{self.host}/{endpoint}'
        data = json.dumps(data) if data else None

        response = getattr(requests, method)(
            url,
            headers=self.headers,
            data=data,
            params=params,
        )
        try:
            response.raise_for_status()
        except requests.exceptions.HTTPError:
            raise Exception(response.content)

        try:
            return response.json()
        except:
            return response.content


class RESTManager:
    """Manager for API access."""

    _from_parent_attrs = []
    _listed_by = None
    _managers = {}
    _obj_cls = None
    _path = ''

    def __init__(self, api, parent=None):
        """Init."""
        self.api = api
        self._parent = parent
        self._module = importlib.import_module(self.__module__)
        self._computed_path = self._compute_path()
        self._create_managers()

    def _compute_path(self):
        """Get the computed path replacing variables."""
        self._parent_attrs = {}

        if self._parent is None or not hasattr(self, '_from_parent_attrs'):
            return self._path

        # Copy the arguments from the parent.
        for attr in self._from_parent_attrs:
            # Handle different names on parent and child.
            # name:parent_name will populate child[name] with parent[parent_name]
            if len(attr.split(':')) > 1:
                attr, parent_attr = attr.split(':', 1)
            else:
                parent_attr = attr

            # Search for sub elements in parent.
            # elem.subelem returns parent.elem.subelem
            value = self._parent
            for key in parent_attr.split('.'):
                try:
                    value = value[key]
                except TypeError:
                    value = getattr(value, key)

            self._parent_attrs[attr] = value

        return self._path.format(**self._parent_attrs)

    def _create_managers(self):
        """Create child managers."""
        managers = getattr(self, "_managers", None)
        if managers is None:
            return

        for attr, cls_name in self._managers.items():
            cls = getattr(self._module, cls_name)
            manager = cls(self.api, parent=self)
            self.__dict__[attr] = manager

    @property
    def path(self):
        """Return computed path."""
        return self._computed_path

    def __getattr__(self, name):
        """Try to get a value here. Otherwise, look in the parent attrs."""
        try:
            return self.__dict__[name]
        except KeyError:
            return self._parent_attrs[name]


class RESTObject:
    """Object to hold the server data."""

    _id = "id"
    _managers = {}

    def __init__(self, manager, attrs):
        """Init."""
        self.manager = manager
        self._attrs = attrs
        self._module = importlib.import_module(self.__module__)
        self._parent_attrs = self.manager._parent_attrs

        self._create_managers()

    def _create_managers(self):
        """Create child managers."""
        managers = getattr(self, "_managers", None)
        if managers is None:
            return

        for attr, cls_name in self._managers.items():
            cls = getattr(self._module, cls_name)
            manager = cls(self.manager.api, parent=self)
            self.__dict__[attr] = manager

    def __getattr__(self, name):
        """Get object attributes."""
        return self.attributes.get(name)

    def __dir__(self):
        """Get all attributes."""
        return super(RESTObject, self).__dir__() + list(self.attributes)

    def __repr__(self):
        """Get object representation."""
        if self.get_id():
            return f'<{self.__class__.__name__} {self._id}:{self.get_id()}>'
        else:
            return f'<{self.__class__.__name__}>'

    @property
    def attributes(self):
        """All attributes plus parent attrs."""
        d = self.__dict__["_attrs"].copy()
        d.update(self.__dict__["_parent_attrs"])
        return d

    def get_id(self):
        """Returns the id of the resource."""
        if self._id is None or self.attributes.get(self._id) is None:
            return None
        return getattr(self, self._id)


class GETMethod:
    # pylint: disable=too-few-public-methods
    """Add GET capabilties."""

    def get(self, id=None, **kwargs):
        # pylint: disable=no-member
        """GET Request."""

        # If the id is set on the kwargs use that.
        if id is None and kwargs.get(self._obj_cls._id):
            id = kwargs.pop(self._obj_cls._id)

        path = f'{self.path}/{id}' if id is not None else self.path
        path = path.format(**kwargs)

        data = self.api.get(path, params=kwargs)
        return self._obj_cls(self, data)


class CREATEMethod:
    # pylint: disable=too-few-public-methods
    """Add CREATE capabilties."""

    def create(self, **kwargs):
        # pylint: disable=no-member
        """CREATE Request."""
        data = self.api.post(self.path, data=kwargs)
        if data:
            return self._obj_cls(self, data)


class List:
    """Generator representing a list of remote objects.

    The object handles the links returned by a query to the API, and will call
    the API again when needed.
    """

    def __init__(self, manager, path, get_next=True, **kwargs):
        """Init."""
        self._manager = manager

        self._listed_by = manager._listed_by
        self._kwargs = kwargs.copy()

        self._query(path, **self._kwargs)
        self._get_next = get_next

    @staticmethod
    def _get_endpoint(url):
        """
        Extract endpoint from the full url.

        The API methods expect only the endpoint, without the protocol, hostname and port.
        """
        if not url:
            return None
        url = urlparse(url)
        return f'{url.path}?{url.query}'.lstrip('/')

    def _query(self, url, **kwargs):
        """Run the query and populate self._data."""
        url = urlparse(url)

        # Get kwarg params and update them with the url query
        # This is necessary to change the pagination parameters
        params = dict(parse_qsl(url.query))
        params.update(kwargs)

        data = self._manager.api.get(url.path, params=params)

        if self._listed_by:
            self._data = data['results'][self._listed_by]
        else:
            self._data = data['results']

        self._previous = self._get_endpoint(data.get('previous'))
        self._next = self._get_endpoint(data.get('next'))
        self._count = data.get('count') or len(self._data)

        self._current = 0

    def __iter__(self):
        """Return iterator."""
        return self

    def __len__(self):
        """
        Return the number of objects.

        If self._get_next is True, return the total number of objects
        reported by the server.
        """
        if self._get_next:
            return int(self._count)
        return len(self._data)

    def __next__(self):
        """Get next item."""
        return self.get_next()

    def __getitem__(self, index):
        """Get an item by index."""
        return self._manager._obj_cls(self._manager, self._data[index])

    def get_next(self):
        """Get next item and query the server if necessary."""
        try:
            item = self[self._current]
            self._current += 1
            return item
        except IndexError:
            pass

        if self._next and self._get_next is True:
            self._query(self._next, **self._kwargs)
            return self.get_next()

        raise StopIteration


class LISTMethod:
    # pylint: disable=too-few-public-methods
    """Add LIST capabilties."""

    def list(self, as_list=True, get_next=True, **kwargs):
        # pylint: disable=no-member
        """
        LIST Request.

        as_list: Return a list or an iterator. This parameter should be set to False
          when the query can return several pages.
        get_next: Follow pagination links to retrieve all results. Setting it to False
          only returns the first page of results.
        """
        path = self.path.format(**kwargs)
        results = List(self, path, get_next, **kwargs)
        if as_list:
            return list(results)
        return results


class DELETEMethod:
    """Add DELETE capabilties."""

    def delete(self, id=None, **kwargs):
        # pylint: disable=no-member
        """DELETE Request."""
        if id is None:
            path = self.path
        else:
            path = f'{self.path}/{id}'
        self.api.delete(path, **kwargs)
        return None


class ObjectDELETEMethod:
    """For RESTObject's that can be deleted."""

    def delete(self, **kwargs):
        # pylint: disable=no-member
        """DELETE Object."""
        self.manager.delete(self.get_id())
