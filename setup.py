#!/usr/bin/python3

from distutils.core import setup

setup(name='generic-rest-client',
      version='0.1',
      description='Easily generate REST API client libs.',
      author='Iñaki Malerba',
      author_email='inaki@malerba.space',
      packages=['restclient'],
      install_requires=open('requirements.txt').read().splitlines(),
     )
