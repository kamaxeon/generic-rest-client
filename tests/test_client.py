import json
import unittest
from unittest import mock
import responses

from restclient import base
from example_app import MyApp, objects


USERS = {
        0: {
            'id': 0,
            'name': 'Bob',
            'emails': {
                0: {'email_id': 0, 'address': 'bob@mail.com'},
                3: {'email_id': 3, 'address': 'bob.s@protonmail.com'},
            },
            'phones': [
                {'phone_id': 0, 'number': 5656456},
            ]
        },
        1: {
            'id': 1,
            'name': 'Alice',
            'emails': {
                1: {'email_id': 1, 'address': 'alice@mail.com'},
                2: {'email_id': 2, 'address': 'alice_other@mail.com'},
            },
            'phones': [
                {'phone_id': 1, 'number': 12345},
            ]
        },
    }


class TestClient(unittest.TestCase):
    # pylint: disable=no-member
    """Test lib methods with example_app/."""

    def setUp(self):
        self.client = MyApp('http://server', None)

    @responses.activate
    def test_list(self):
        """Test LISTMethod."""
        responses.add(
            responses.GET,
            'http://server/user',
            json={
                'results': {'users': list(USERS.values())},
                'count': 2,
                'previous': None,
                'next': None,
            }
        )

        users = self.client.user.list()
        self.assertEqual(list, type(users))
        self.assertEqual(objects.User, type(users[0]))
        self.assertEqual('Bob', users[0].name)
        self.assertEqual(1, len(users[0].phones))

    @responses.activate
    def test_get(self):
        """Test GETMethod."""
        responses.add(responses.GET, 'http://server/user/0', json=USERS[0])

        user = self.client.user.get(0)
        self.assertEqual(objects.User, type(user))
        self.assertEqual('Bob', user.name)
        self.assertEqual(1, len(user.phones))

    @responses.activate
    def test_get_different(self):
        """Test GETMethod."""
        responses.add(responses.GET, 'http://server/user/1', json=USERS[1])

        user = self.client.user.get(1)
        self.assertEqual('Alice', user.name)

    @responses.activate
    def test_get_with_kwarg(self):
        """Test GETMethod with ID as keyword argument."""
        responses.add(responses.GET, 'http://server/user/1', json=USERS[1])

        user = self.client.user.get(id=1)
        self.assertEqual('Alice', user.name)

    @responses.activate
    def test_use_manager_get(self):
        """Test GETMethod in a nested manager."""
        responses.add(responses.GET, 'http://server/user/0', json=USERS[0])
        responses.add(responses.GET, 'http://server/user/0/email/0', json=USERS[0]['emails'][0])
        responses.add(responses.GET, 'http://server/user/0/email/3', json=USERS[0]['emails'][3])

        user = self.client.user.get(0)
        email = user.email.get(0)
        self.assertEqual(objects.UserEmail, type(email))
        self.assertEqual('bob@mail.com', email.address)

        email = user.email.get(3)
        self.assertEqual('bob.s@protonmail.com', email.address)

    @responses.activate
    def test_use_manager_list(self):
        """Test LISTMethod in a nested manager."""
        responses.add(responses.GET, 'http://server/user/0', json=USERS[0])
        responses.add(
            responses.GET,
            'http://server/user/0/email',
            json={
                'results': {'emails': list(USERS[0]['emails'].values())},
                'count': 2,
                'previous': None,
                'next': None,
            }
        )

        user = self.client.user.get(0)
        emails = user.email.list()
        self.assertEqual(objects.UserEmail, type(emails[0]))
        self.assertEqual(2, len(emails))
        self.assertEqual('bob@mail.com', emails[0].address)
        self.assertEqual('bob.s@protonmail.com', emails[1].address)

    @responses.activate
    def test_create(self):
        """Test CREATEMethod."""
        user = {'id': 2, 'name': 'Joey', 'emails': {}, 'phones': []}
        responses.add(responses.POST, 'http://server/user', json=user)

        new_user = self.client.user.create(name=user['name'])
        self.assertEqual(user['name'], new_user.name)

    @responses.activate
    def test_delete(self):
        """Test DELETEMethod."""
        responses.add(responses.GET, 'http://server/user/0', json=USERS[0])
        responses.add(responses.DELETE, 'http://server/user/0')

        user = self.client.user.get(0)
        user.delete()


class TestRequests(unittest.TestCase):
    # pylint: disable=no-member
    """Test requests are performed correctly."""

    def setUp(self):
        self.client = MyApp('http://server', 'my-secret-token')

    @responses.activate
    def test_get_params(self):
        responses.add(responses.GET, 'http://server/user/1234', json=USERS[0])

        user = self.client.user.get(1234, some_param=True, some_other_param=['a', 'b', 'c'])

        self.assertEqual(responses.calls[0].request.headers['Authorization'], 'Token my-secret-token')
        self.assertEqual(responses.calls[0].request.headers['Content-Type'], 'application/json')
        self.assertEqual(
            responses.calls[0].request.url,
            'http://server/user/1234?some_param=True&some_other_param=a&some_other_param=b&some_other_param=c'
        )

    @responses.activate
    def test_list_params(self):
        responses.add(
            responses.GET,
            'http://server/user',
            json={
                'results': {'users': []},
                'count': 0,
                'previous': None,
                'next': None,
            }
        )

        user = self.client.user.list(some_param=True, some_other_param=['a', 'b', 'c'])

        self.assertEqual(responses.calls[0].request.headers['Authorization'], 'Token my-secret-token')
        self.assertEqual(responses.calls[0].request.headers['Content-Type'], 'application/json')
        self.assertEqual(
            responses.calls[0].request.url,
            'http://server/user?some_param=True&some_other_param=a&some_other_param=b&some_other_param=c'
        )

    @responses.activate
    def test_create_params(self):
        responses.add(responses.POST, 'http://server/user', json=USERS[0])

        self.client.user.create(name='Rachel')

        self.assertEqual(responses.calls[0].request.headers['Authorization'], 'Token my-secret-token')
        self.assertEqual(responses.calls[0].request.headers['Content-Type'], 'application/json')
        self.assertEqual(responses.calls[0].request.url, 'http://server/user')
        self.assertEqual(responses.calls[0].request.body, '{"name": "Rachel"}')

    @responses.activate
    def test_delete(self):
        responses.add(responses.DELETE, 'http://server/user/1')
        responses.add(responses.GET, 'http://server/user/1', json=USERS[1])

        user = self.client.user.get(1)

        user.delete()

        self.assertEqual(responses.calls[1].request.headers['Authorization'], 'Token my-secret-token')
        self.assertEqual(responses.calls[1].request.headers['Content-Type'], 'application/json')
        self.assertEqual(responses.calls[1].request.url, 'http://server/user/1')
        self.assertEqual(responses.calls[1].request.method, 'DELETE')

    @responses.activate
    def test_list_inherited(self):
        """Test elements in a list has the correct inherited parameters."""
        responses.add(
            responses.GET,
            'http://server/user',
            json={
                'results': {'users': list(USERS.values())},
                'count': 2,
                'previous': None,
                'next': None,
            }
        )
        responses.add(
            responses.GET,
            'http://server/user/0/email',
            json={
                'results': {'emails': list(USERS[0]['emails'].values())},
                'count': 2,
                'previous': None,
                'next': None,
            }
        )
        responses.add(
            responses.GET,
            'http://server/user/1/email',
            json={
                'results': {'emails': list(USERS[0]['emails'].values())},
                'count': 2,
                'previous': None,
                'next': None,
            }
        )


        users = self.client.user.list()
        users[0].email.list()
        users[1].email.list()

        self.assertEqual(responses.calls[0].request.url, 'http://server/user')
        self.assertEqual(responses.calls[1].request.url, 'http://server/user/0/email')
        self.assertEqual(responses.calls[2].request.url, 'http://server/user/1/email')

    @responses.activate
    def test_list_as_iterator(self):
        responses.add(
            responses.GET,
            'http://server/user',
            match_querystring=True,
            json={
                'results': {'users': [USERS[0]]},
                'count': 3,
                'previous': None,
                'next': 'http://server/user?offset=1',
            }
        )
        responses.add(
            responses.GET,
            'http://server/user?offset=1',
            match_querystring=True,
            json={
                'results': {'users': [USERS[1]]},
                'count': 3,
                'previous': 'http://server/user?offset=0',
                'next': 'http://server/user?offset=2',
            }
        )
        responses.add(
            responses.GET,
            'http://server/user?offset=2',
            match_querystring=True,
            json={
                'results': {'users': [USERS[1]]},
                'count': 3,
                'previous': 'http://server/user?offset=1',
                'next': None,
            }
        )

        users = self.client.user.list()
        self.assertEqual(3, len(users))
        self.assertEqual(
            [USERS[0]['id'], USERS[1]['id'], USERS[1]['id']],
            [user.id for user in users]
        )

    @responses.activate
    def test_list_dont_get_next(self):
        responses.add(
            responses.GET,
            'http://server/user',
            match_querystring=True,
            json={
                'results': {'users': [USERS[0]]},
                'count': 3,
                'previous': None,
                'next': 'http://server/user?offset=1',
            }
        )

        users = self.client.user.list(get_next=False)
        self.assertEqual(1, len(users))

    @responses.activate
    def test_list_as_list(self):
        responses.add(
            responses.GET,
            'http://server/user',
            match_querystring=True,
            json={
                'results': {'users': [USERS[0]]},
                'count': 3,
                'previous': None,
                'next': 'http://server/user?offset=1',
            }
        )

        users = self.client.user.list(as_list=False, get_next=False)
        self.assertTrue(isinstance(users, base.List))
        self.assertEqual(1, len(users))
