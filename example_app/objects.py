"""API Objects."""
from typing import List

from restclient import RESTManager, RESTObject
from restclient import GETMethod, LISTMethod, CREATEMethod, DELETEMethod, ObjectDELETEMethod


class UserEmail(RESTObject):
    _id = 'email_id'


class UserEmailManager(RESTManager, GETMethod, LISTMethod):
    _obj_cls = UserEmail
    _path = 'user/{user_id}/email'
    _listed_by = 'emails'
    _from_parent_attrs = ('user_id:id', )


class UserPhone(RESTObject):
    _id = 'phone_id'


class UserPhoneManager(RESTManager, GETMethod):
    _obj_cls = UserPhone
    _path = 'user/{user_id}/phone'


class User(RESTObject, ObjectDELETEMethod):
    _managers = {'email': 'UserEmailManager'}


class UserManager(RESTManager, GETMethod, LISTMethod, CREATEMethod, DELETEMethod):
    _obj_cls = User
    _path = 'user'
    _listed_by = 'users'
