"""API Client."""
from restclient import APIManager
from . import objects

class MyApp:
    # pylint: disable=too-few-public-methods
    """MyApp main class."""

    def __init__(self, host, token):
        api = APIManager(host, token)

        # Define root methods here.
        self.user = objects.UserManager(api)
